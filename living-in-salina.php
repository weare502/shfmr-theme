<?php
/**
 * Template Name: Living in Salina
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = [ 'living-in-salina.twig' ];

Timber::render( $templates, $context );