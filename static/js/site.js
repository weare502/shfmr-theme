(function($) {
    $(document).ready(function() {
		let $body = $('body');

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').fadeToggle(550);

				// fade out main site chunks when menu is opened
				$('header').fadeToggle(450); // <header>
				$('.swoop').fadeToggle(450); // main swoop wrapper
				$('.content-wrapper').fadeToggle(450); // main content wrapper
				$('.above-footer').fadeToggle(450); // content area above the footer
				$('footer').fadeToggle(450); // <footer>
				$('.lower-footer').fadeToggle(450); // attribution below main footer
			});
		});

		// mobile nav core functions - sub-menu open/expand functions and handlers for back button option
		$(function mobileNavigationRouter() {
			if( $body.width() < 1024) {
				// add back button at top of each sub-menu
				$('.sub-menu').prepend('<li><button id="mobile-back-button" class="menu-back"> Back</button></li>');

				// close sub-menu when back button is clicked
				$('li button.menu-back').on('click', function(e) {
					var $menu = $(this).parents('.sub-menu:first');
					$menu.removeClass('sub-menu-open');
					e.stopPropagation(); // prevent event from bubbling back up
				});

				// expand function
				$('.menu-item-has-children').click(function(e) {
					var $sub = $(this).find('.sub-menu:first');

					// if it has children, but doesn't have it's menu open, do not follow the link
					if( ! $sub.hasClass('sub-menu-open')) {
						e.preventDefault(); // don't follow links on first click
					}

					$sub.addClass('sub-menu-open');
					e.stopPropagation(); // prevent from bubbling back up
				});
			}
		});

		// dropdown block - controls and aria events for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

		// remove inline style tags from wiki pages search bar, breadcrumbs and links
		$('.epkb-doc-search-container').attr('style', '');
		$('.eckb-breadcrumb-link span').attr('style', '');
		$('span.eckb-breadcrumb-link').attr('style', '');

		// logic and controls for custom videos
		$(function videoToggle() {
			// swoop player controls
			var $swoop_player = $('#large-video-player').get(0);
			var $large_play_button = $('#large-play-button');

			// swoop header players on the page
			$large_play_button.click(function() {
				$(this).hide(); // hide button svg

				if($swoop_player.paused == true) {
					$swoop_player.play();
					$swoop_player.controls = true; // enable controls once video is playing
				} else {
					$swoop_player.pause();
				}
			});
		});
	}); // end Document.Ready
})(jQuery);