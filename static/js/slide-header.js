(function($) {
    $(document).ready(function() {

		$(function sliderInit() {
			var $slick_entry = $('.slider-entry');

			$slick_entry.slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,
				accessibility: true,
				draggable: true,
				dots: false,
				arrows: true,
				prevArrow: $('.prev-arrow'),
				nextArrow: $('.next-arrow'),
			});
		}); // end sliderInit

	}); // end Document.Ready
})(jQuery);