<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'shfmr' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$important_info_block = [
		'name' => 'important-info-block',
		'title' => __( 'Important Info', 'shfmr' ),
		'description' => __( 'Creates an important message "banner" with a purple left border, and text on the right.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'info',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'important', 'info', 'message', 'purple' ]
	];
	acf_register_block_type( $important_info_block );

	$white_button_block = [
		'name' => 'white-button-block',
		'title' => __( 'White Button', 'shfmr' ),
		'description' => __( 'Creates a white button with purple text.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'white' ]
	];
	acf_register_block_type( $white_button_block );

	// bordered purple block, white background
	$purple_button_block = [
		'name' => 'purple-button-block',
		'title' => __( 'Purple Button', 'shfmr' ),
		'description' => __( 'Creates a purple border button with purple text.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'purple', 'bordered' ]
	];
	acf_register_block_type( $purple_button_block );

	$solid_purple_button_block = [
		'name' => 'solid-purple-button-block',
		'title' => __( 'Solid Purple Button', 'shfmr' ),
		'description' => __( 'Creates a button with a purple background and white text.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'purple', 'solid' ]
	];
	acf_register_block_type( $solid_purple_button_block );

	// bordered mint button, white background
	$mint_button_block = [
		'name' => 'mint-button-block',
		'title' => __( 'Mint Button', 'shfmr' ),
		'description' => __( 'Creates a mint bordered button with mint text.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'mint', 'green', 'bordered' ]
	];
	acf_register_block_type( $mint_button_block );

	// solid mint button, reverse of bordered
	$solid_mint_button_block = [
		'name' => 'solid-mint-button-block',
		'title' => __( 'Solid Mint Button', 'shfmr' ),
		'description' => __( 'Creates a button with a mint background and white text.', 'shfmr' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'shfmr-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'mint', 'green', 'solid' ]
	];
	acf_register_block_type( $solid_mint_button_block );
endif;